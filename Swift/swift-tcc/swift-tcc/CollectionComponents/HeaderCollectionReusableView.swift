//
//  HeaderCollectionReusableView.swift
//  swift-tcc
//
//  Created by Pedro Veloso on 01/05/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

import UIKit

class HeaderCollectionReusableView: UICollectionReusableView {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.groupTableViewBackground
    }
    
    func setup(title: String, subtitle: String) {
        titleLabel.text = title
        subtitleLabel.isHidden = subtitle.isEmpty
        subtitleLabel.text = subtitle
    }
}
