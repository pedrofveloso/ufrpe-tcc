//
//  BallCollectionViewCell.swift
//  swift-tcc
//
//  Created by Pedro Veloso on 01/05/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

import UIKit

class BallCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var numberLabel: UILabel! {
        didSet {
            numberLabel.layer.cornerRadius = numberLabel.frame.height/2
            numberLabel.layer.masksToBounds = true
            numberLabel.textColor = .white
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setup(number: String, color: UIColor) {
        numberLabel.text = number
        numberLabel.backgroundColor = color
    }
}
