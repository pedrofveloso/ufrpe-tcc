//
//  Theme.swift
//  swift-tcc
//
//  Created by Pedro Veloso on 19/05/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

import UIKit

enum Lotery: String {
    case lotofacil = "lotofacil"
    case lotomania = "lotomania"
    case megasena = "mega-sena"
    case quina = "quina"
    
    var numberOfBalls: Int {
        switch self {
        case .lotofacil:
            return 15
        case .lotomania:
            return 20
        case .megasena:
            return 6
        default:
            return 5
        }
    }
    
    var color: UIColor {
        switch self {
        case .lotofacil:
            return UIColor(red: 88/255, green: 86/255, blue: 214/255, alpha: 1)
        case .lotomania:
            return UIColor(red: 1, green: 149/255, blue: 0, alpha: 1)
        case .megasena:
            return UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
        default:
            return UIColor(red: 0/255, green: 122/255, blue: 1, alpha: 1)
        }
    }
}
