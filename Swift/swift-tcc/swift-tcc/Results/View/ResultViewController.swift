//
//  ResultViewController.swift
//  swift-tcc
//
//  Created by Pedro Veloso on 28/04/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

import UIKit

class ResultViewController: UIViewController {
    //MARK: - IBOutlets
    @IBOutlet weak var resultCollectionView: UICollectionView! {
        didSet {
            resultCollectionView.dataSource = self
            resultCollectionView.delegate = self
            resultCollectionView.register(UINib(nibName: "BallCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "resultCell")
            resultCollectionView.register(UINib(nibName: "HeaderCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "resultView")
        }
    }
    
    //MARK: - Properties
    var presenter: ResultPresenterDelegate?
    
    //MARK: - Init
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - UIViewControllers
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Resultados"
        presenter?.viewDidLoad()
    }
}

extension ResultViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return presenter?.numberOfSections ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter?.numberOfItems(for: section) ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        guard let repo = presenter,
            let view = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "resultView", for: indexPath) as? HeaderCollectionReusableView
            else { return UICollectionReusableView() }
        view.setup(title: repo.getTitle(forContest: indexPath.section),
                   subtitle: repo.getSubtitle(fromContest: indexPath.section))
        return view
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let presenter = presenter,
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "resultCell", for: indexPath) as? BallCollectionViewCell
            else { return UICollectionViewCell() }
    
        cell.setup(number: presenter.getBall(fromContest: indexPath.section, forIndex: indexPath.row), color: presenter.loteryColor)
        return cell
    }
}

extension ResultViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 40)
    }
}
