//
//  ResultPresenterDelegate.swift
//  swift-tcc
//
//  Created by Pedro Veloso on 19/05/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

import UIKit

protocol ResultPresenterDelegate: AnyObject {
    
    init(repository: Repository)
    
    var numberOfSections: Int { get }
    func numberOfItems(for index: Int) -> Int
    var loteryColor: UIColor { get }
    
    func viewDidLoad()
    
    func getBall(fromContest contest: Int, forIndex index: Int) -> String
    func getSubtitle(fromContest contest: Int) -> String
    func getTitle(forContest contest: Int) -> String
}
