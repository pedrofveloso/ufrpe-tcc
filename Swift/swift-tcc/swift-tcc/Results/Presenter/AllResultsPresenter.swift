//
//  AllResultsPresenter.swift
//  swift-tcc
//
//  Created by Pedro Veloso on 19/05/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

import UIKit

class AllResultsPresenter {
    private var repository: Repository
    
    required init(repository: Repository) {
        self.repository = repository
    }
}

extension AllResultsPresenter: ResultPresenterDelegate {
    func viewDidLoad() {}
    
    var numberOfSections: Int {
        return repository.results.count
    }
    
    func numberOfItems(for index: Int) -> Int {
        return repository.currentLotery.numberOfBalls
    }
    
    var loteryColor: UIColor {
        return repository.currentLotery.color
    }
    
    func getBall(fromContest contest: Int, forIndex index: Int) -> String {
        return "\(repository.results[contest]["bola \(index + 1)"] ?? "")"
    }
    
    func getSubtitle(fromContest contest: Int) -> String {
        let date = repository.results[contest]["Data"] as? String ?? ""
        return "Realizado no dia: \(date)"
    }
    
    func getTitle(forContest contest: Int) -> String {
        return "Concurso número: \(repository.results[contest]["Concurso"] ?? "")"
    }
    
}
