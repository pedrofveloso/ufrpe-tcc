//
//  MoreDrawnNumbersPresenter.swift
//  swift-tcc
//
//  Created by Pedro Veloso on 19/05/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

import UIKit

class MostDrawnNumbersPresenter {
    private let repository: Repository
    private var results: [String]!
    
    required init(repository: Repository) {
        self.repository = repository
    }
    
    private func mountResultArray() -> [String] {
        var numbersByQty: [String: Int] = [:]
        repository.results.forEach { (dict) in
            for index in 1...repository.currentLotery.numberOfBalls {
                let number = dict["bola \(index)"] as! Int
                if let key = numbersByQty["\(number)"] {
                    numbersByQty["\(number)"] = key + 1
                } else {
                    numbersByQty["\(number)"] = 1
                    continue
                }
            }
        }
        
        let resultArray = numbersByQty.sorted { (dict1, dict2) -> Bool in
            dict1.value > dict2.value
        }
        
        return resultArray.map{ $0.key }
    }
}

extension MostDrawnNumbersPresenter: ResultPresenterDelegate {
    var numberOfSections: Int {
        return 1
    }
    
    func numberOfItems(for index: Int) -> Int {
        return repository.currentLotery.numberOfBalls
    }
    
    var loteryColor: UIColor {
        return repository.currentLotery.color
    }
    
    func viewDidLoad() {
        results = mountResultArray()
    }
    
    func getBall(fromContest contest: Int, forIndex index: Int) -> String {
        return results[index]
    }
    
    func getSubtitle(fromContest contest: Int) -> String {
        return ""
    }
    
    func getTitle(forContest contest: Int) -> String {
        return "Números mais sorteados:"
    }
}
