//
//  ProbabilitiesPresenter.swift
//  swift-tcc
//
//  Created by Pedro Veloso on 19/05/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

import UIKit

class ProbabilitiesPresenter {
    let repository: Repository
    
    private var result: [String: [Int]]
    private var keys: [String]
    
    required init(repository: Repository) {
        self.repository = repository
        self.result = [:]
        self.keys = []
    }
}

extension ProbabilitiesPresenter: ResultPresenterDelegate {
    var numberOfSections: Int {
        return keys.count
    }
    
    func numberOfItems(for index: Int) -> Int {
        let key = keys[index]
        return result[key]?.count ?? 0
    }
    
    var loteryColor: UIColor {
        return repository.currentLotery.color
    }
    
    func viewDidLoad() {
        var numbersByQty: [String: Double] = [:]
        let total:Double = Double(repository.results.count * repository.currentLotery.numberOfBalls)
        repository.results.forEach { (dict) in
            for index in 1...repository.currentLotery.numberOfBalls {
                let number = dict["bola \(index)"] as! Int
                if let key = numbersByQty["\(number)"] {
                    numbersByQty["\(number)"] = key+1
                } else {
                    numbersByQty["\(number)"] = 1
                    continue
                }
            }
        }
        var numbersByProb: [String: [Int]] = [:]
        numbersByQty.forEach { (dict) in
            
            guard let key = Int(dict.key) else {
                return
            }
            
            let value = dict.value/total
            if numbersByProb["\(value)"] == nil {
                numbersByProb["\(value)"] = []
            }
            numbersByProb["\(value)"]!.append(key)
        }
        
        result = numbersByProb
        
        let sortedResult = numbersByProb.sorted(by: { (dict1, dict2) -> Bool in
            dict1.key > dict2.key
        })
        
        keys = sortedResult.map { $0.key }
    }
    
    func getBall(fromContest contest: Int, forIndex index: Int) -> String {
        let key = keys[contest]
        guard let ball = result[key]?[index] else { return "" }
        return "\(ball)"
    }
    
    func getSubtitle(fromContest contest: Int) -> String {
        return ""
    }
    
    func getTitle(forContest contest: Int) -> String {
        guard let prob = Double(keys[contest]) else { return "Probabilidade não encontrada" }
        return "Probabilidade de \(prob * 100)%"
    }
}
