//
//  ViewController.swift
//  swift-tcc
//
//  Created by Pedro Veloso on 28/04/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
//    MARK: - Properties
    var repository = Repository(lotery: .megasena)
    let options = ["Resultados", "Mais sorteados", "Probabilidades"]
    
    //MARK: - IBOutlets
    @IBOutlet weak var segmentedControl: UISegmentedControl! {
        didSet {
            segmentedControl.removeAllSegments()
            segmentedControl.insertSegment(withTitle: "Mega-Sena", at: 0, animated: true)
            segmentedControl.insertSegment(withTitle: "Quina", at: 1, animated: true)
            segmentedControl.insertSegment(withTitle: "Lotofacil", at: 2, animated: true)
            segmentedControl.insertSegment(withTitle: "Lotomania", at: 3, animated: true)
            
            segmentedControl.selectedSegmentIndex = 0
        }
    }
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.dataSource = self
            tableView.delegate = self
        }
    }
    
    //MARK: - IBActions
    @IBAction func segmentedControlValueChanged(_ sender: UISegmentedControl) {
        guard let lotery = Lotery(rawValue: sender.titleForSegment(at: sender.selectedSegmentIndex)?.lowercased() ?? "") else { return }
        navigationItem.title = lotery.rawValue.capitalized
        repository = Repository(lotery: lotery)
    }

    //MARK: - UIViewControllers
    override func viewDidLoad() {
        super.viewDidLoad()
        segmentedControlValueChanged(segmentedControl)
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return options.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
        cell.textLabel?.text = options[indexPath.row]
        cell.accessoryType = .disclosureIndicator
        cell.selectionStyle = .none
        return cell
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = ResultViewController(nibName: "ResultViewController", bundle: nil)
        switch indexPath.row {
        case 0:
            vc.presenter = AllResultsPresenter(repository: repository)
        case 1:
            vc.presenter = MostDrawnNumbersPresenter(repository: repository)
        case 2:
            vc.presenter = ProbabilitiesPresenter(repository: repository)
        default:
            break
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
