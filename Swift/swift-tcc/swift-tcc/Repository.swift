//
//  Repository.swift
//  swift-tcc
//
//  Created by Pedro Veloso on 28/04/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

import Foundation
import UIKit

class Repository {
    let currentLotery: Lotery
    let results: [[String: Any]]
    
    init(lotery: Lotery) {
        currentLotery = lotery
        guard let path = Bundle.main.path(forResource: "resultados-\(lotery.rawValue)", ofType: "json") else {
            results = []
            return
        }
        do {
            let data = try Data.init(contentsOf: URL(fileURLWithPath: path))
            let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [[String: Any]]
            results = json ?? []
        } catch {
            fatalError("Invalid json file")
        }
    }
}
