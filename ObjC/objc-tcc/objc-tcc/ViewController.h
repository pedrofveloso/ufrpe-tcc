//
//  ViewController.h
//  objc-tcc
//
//  Created by Pedro Veloso on 16/06/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "Results/Presenter/AllResultsPresenter.m"

NS_ASSUME_NONNULL_BEGIN

@interface ViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@end

NS_ASSUME_NONNULL_END
