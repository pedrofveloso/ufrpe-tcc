//
//  Theme.m
//  objc-tcc
//
//  Created by resource on 19/06/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

#import "Theme.h"

@implementation Theme

+(NSString *)stringFromLotery:(enum Lotery) lotery {
    switch (lotery) {
        case lotofacil: return @"lotofacil";
        case lotomania: return @"lotomania";
        case megasena: return @"mega-sena";
        case quina: return @"quina";
    }
}

+(UIColor *)colorFromLotery:(enum Lotery) lotery {
    switch (lotery) {
        case lotofacil: return [[UIColor alloc] initWithRed:88.0/255.0 green:86.0/255.0 blue:241.0/255.0 alpha:1];
        case lotomania: return [[UIColor alloc] initWithRed:1 green:149.0/255.0 blue:0 alpha:1];
        case megasena: return [[UIColor alloc] initWithRed:76.0/255.0 green:217.0/255.0 blue:100.0/255.0 alpha:1];
        case quina: return [[UIColor alloc] initWithRed:0 green:122.0/255.0 blue:1 alpha:1];
    }
}

+ (int)numberOfBalls:(enum Lotery)lotery {
    switch (lotery) {
        case lotofacil: return 15;
        case lotomania: return 20;
        case megasena: return 6;
        case quina: return 5;
    }
}

@end
