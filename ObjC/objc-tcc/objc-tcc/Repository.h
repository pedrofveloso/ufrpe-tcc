//
//  Repository.h
//  objc-tcc
//
//  Created by resource on 19/06/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Theme.h"
NS_ASSUME_NONNULL_BEGIN

@interface Repository : NSObject
@property enum Lotery currentLotery;
@property NSArray* results;
- (instancetype)initWithLotery: (enum Lotery) lotery;
@end

NS_ASSUME_NONNULL_END
