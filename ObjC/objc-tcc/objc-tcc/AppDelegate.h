//
//  AppDelegate.h
//  objc-tcc
//
//  Created by Pedro Veloso on 16/06/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

