//
//  ViewController.m
//  objc-tcc
//
//  Created by Pedro Veloso on 16/06/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

#import "ViewController.h"
#import "Repository.h"
#import "Results/View/ResultsViewController.h"
#import "Results/Presenter/AllResultsPresenter.h"
#import "Results/Presenter/MostDrawnNumbersPresenter.h"
#import "Results/Presenter/ProbabilitiesPresenter.h"

@interface ViewController()
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation ViewController {
    NSArray* options;
    Repository* repository;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    options = [NSArray arrayWithObjects:@"Resultados", @"Mais sorteados", @"Probabilidades", nil];
    [self setupComponents];
    [self segmentControlValueChanged:_segmentedControl];
}

- (void) setupComponents {
    [_segmentedControl removeAllSegments];
    [_segmentedControl insertSegmentWithTitle:@"Mega-Sena" atIndex:0 animated:YES];
    [_segmentedControl insertSegmentWithTitle:@"Quina" atIndex:1 animated:YES];
    [_segmentedControl insertSegmentWithTitle:@"Lotofacil" atIndex:2 animated:YES];
    [_segmentedControl insertSegmentWithTitle:@"Lotomania" atIndex:3 animated:YES];
    _segmentedControl.selectedSegmentIndex = 0;
    _tableView.delegate = self;
    _tableView.dataSource = self;
}

- (IBAction)segmentControlValueChanged:(UISegmentedControl *)sender {
    enum Lotery lotery;
    switch (sender.selectedSegmentIndex) {
        case 0:
            lotery = megasena;
            break;
        case 1:
            lotery = quina;
            break;
        case 2:
            lotery = lotomania;
            break;
        default:
            lotery = lotofacil;
            break;
    }
    self.navigationItem.title = [Theme stringFromLotery:lotery].capitalizedString;
    repository = [[Repository alloc] initWithLotery:lotery];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return options.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [[UITableViewCell alloc] initWithStyle: UITableViewCellStyleDefault reuseIdentifier: NULL];
    cell.textLabel.text = options[indexPath.row];
    [cell setAccessoryType: UITableViewCellAccessoryDisclosureIndicator];
    [cell setSelectionStyle: UITableViewCellSelectionStyleNone];
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc] init];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ResultsViewController* result = [[ResultsViewController alloc] initWithNibName:@"ResultsViewController" bundle:nil];
    
    if (indexPath.row == 0) {
        result.presenter = [[AllResultsPresenter alloc] initWith: repository];;
    } else if (indexPath.row == 1) {
        result.presenter = [[MostDrawnNumbersPresenter alloc] initWith: repository];
    } else {
        result.presenter = [[ProbabilitiesPresenter alloc] initWith: repository];
    }
    
    [self.navigationController pushViewController:result animated:YES];
}

@end
