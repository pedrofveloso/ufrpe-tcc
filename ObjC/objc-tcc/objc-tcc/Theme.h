//
//  Theme.m
//  objc-tcc
//
//  Created by Pedro Veloso on 16/06/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

#import <UIKit/UIKit.h>

enum Lotery {
    lotofacil,
    lotomania,
    megasena,
    quina
    
};

@interface Theme : NSObject
+(NSString*) stringFromLotery:(enum Lotery) lotery;
+(UIColor*) colorFromLotery:(enum Lotery) lotery;
+(int) numberOfBalls:(enum Lotery) lotery;
@end
