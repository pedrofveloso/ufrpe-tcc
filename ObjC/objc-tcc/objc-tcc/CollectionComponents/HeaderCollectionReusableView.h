//
//  HeaderCollectionReusableView.h
//  objc-tcc
//
//  Created by resource on 19/06/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HeaderCollectionReusableView : UICollectionReusableView
-(void) setupWithTitle: (NSString*)title andSubtitle: (NSString*)subtitle;
@end

NS_ASSUME_NONNULL_END
