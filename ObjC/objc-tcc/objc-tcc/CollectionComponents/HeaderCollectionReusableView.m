//
//  HeaderCollectionReusableView.m
//  objc-tcc
//
//  Created by resource on 19/06/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

#import "HeaderCollectionReusableView.h"

@interface HeaderCollectionReusableView()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;

@end

@implementation HeaderCollectionReusableView

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor = UIColor.groupTableViewBackgroundColor;
}

- (void)setupWithTitle:(NSString *)title andSubtitle:(NSString *)subtitle {
    self.titleLabel.text = title;
    [self.subtitleLabel setHidden: !subtitle.length];
    self.subtitleLabel.text = subtitle;
}

@end
