//
//  BallCollectionViewCell.h
//  objc-tcc
//
//  Created by resource on 19/06/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BallCollectionViewCell : UICollectionViewCell
- (void)setupComponentWithNumber: (NSString*)number andColor: (UIColor*)color;
@end

NS_ASSUME_NONNULL_END
