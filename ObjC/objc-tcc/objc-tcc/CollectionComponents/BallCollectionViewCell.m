//
//  BallCollectionViewCell.m
//  objc-tcc
//
//  Created by resource on 19/06/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

#import "BallCollectionViewCell.h"

@interface BallCollectionViewCell()
@property (weak, nonatomic) IBOutlet UILabel *numberLabel;

@end

@implementation BallCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.numberLabel.layer.cornerRadius = self.numberLabel.frame.size.height/2;
    self.numberLabel.layer.masksToBounds = YES;
    self.numberLabel.textColor = UIColor.whiteColor;
}

- (void)setupComponentWithNumber: (NSString*)number andColor: (UIColor*)color {
    self.numberLabel.backgroundColor = color;
    self.numberLabel.text = number;
}

@end
