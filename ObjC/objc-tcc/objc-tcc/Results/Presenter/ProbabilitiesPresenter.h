//
//  ProbabilitiesPresenter.h
//  objc-tcc
//
//  Created by resource on 21/06/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ResultPresenterDelegate.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProbabilitiesPresenter : NSObject <ResultPresenterDelegate>

@end

NS_ASSUME_NONNULL_END
