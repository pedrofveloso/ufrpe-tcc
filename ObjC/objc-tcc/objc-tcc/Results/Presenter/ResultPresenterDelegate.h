//
//  ResultPresenterDelegate.h
//  objc-tcc
//
//  Created by resource on 19/06/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Repository.h"

NS_ASSUME_NONNULL_BEGIN

@protocol ResultPresenterDelegate <NSObject>
- (instancetype) initWith: (Repository*) repo;

@property (readonly) int numberOfSections;
@property (readonly) UIColor* loteryColor;

- (void) viewDidLoad;
- (int) numberOfItemsFor:(int) index;
- (NSString*) getBallFromContest: (int)contest forIndex:(int) index;
- (NSString*) getSubtitleFromContest: (int)contest;
- (NSString*) getTitleForContest: (int)contest;

@end

NS_ASSUME_NONNULL_END
