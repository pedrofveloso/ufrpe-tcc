//
//  AllResultsPresenter.m
//  objc-tcc
//
//  Created by resource on 19/06/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

#import "AllResultsPresenter.h"

@implementation AllResultsPresenter {
    Repository* repository;
}

- (nonnull instancetype)initWith:(nonnull Repository *)repo {
    
    if (self) {
        repository = repo;
    }
    
    return self;
}

@synthesize numberOfSections;
@synthesize loteryColor;

- (int)numberOfSections {
    return (int)[repository.results count];
}

- (UIColor *)loteryColor {
    return [Theme colorFromLotery: repository.currentLotery];
}

- (nonnull NSString *)getBallFromContest:(int)contest forIndex:(int)index {
    NSString* key = [NSString stringWithFormat:@"bola %d", index + 1];
    return [NSString stringWithFormat:@"%@", repository.results[contest][key]];
}

- (nonnull NSString *)getSubtitleFromContest:(int)contest {
    NSString* subtitle = repository.results[contest][@"Data"];
    return [NSString stringWithFormat:@"Realizado no dia: %@", subtitle];
}

- (nonnull NSString *)getTitleForContest:(int)contest {
    NSString* title = repository.results[contest][@"Concurso"];
    return [NSString stringWithFormat:@"Concurso número: %@", title];
}

- (int)numberOfItemsFor:(int)index {
    return [Theme numberOfBalls: repository.currentLotery];
}

- (void)viewDidLoad {}

@end
