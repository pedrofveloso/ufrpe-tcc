//
//  ProbabilitiesPresenter.m
//  objc-tcc
//
//  Created by resource on 21/06/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

#import "ProbabilitiesPresenter.h"

@implementation ProbabilitiesPresenter {
    Repository* repository;
    NSDictionary* results;
    NSArray* keys;
}

- (instancetype)initWith:(Repository *)repo {
    if (self) {
        repository = repo;
    }
    
    return self;
}

@synthesize numberOfSections;
- (int)numberOfSections {
    return (int)keys.count;
}

@synthesize loteryColor;
- (UIColor *)loteryColor {
    return [Theme colorFromLotery: repository.currentLotery];
}

-(void)viewDidLoad {
    NSMutableDictionary* numbersByQty = [[NSMutableDictionary alloc] initWithDictionary:@{}];
    double total = (double)(repository.results.count * [Theme numberOfBalls: repository.currentLotery]);
    for (NSDictionary* dict in repository.results) {
        for (int index = 1; index <= [Theme numberOfBalls:repository.currentLotery]; index++) {
            NSString* dictKey = [NSString stringWithFormat:@"bola %d", index];
            NSString* numberAsString = [NSString stringWithFormat:@"%@", dict[dictKey]];
            if(numbersByQty[numberAsString]) {
                int key = (int)[[numbersByQty objectForKey:numberAsString] integerValue] + 1;
                numbersByQty[numberAsString] = [NSNumber numberWithLong: key];
            } else {
                numbersByQty[numberAsString] = [NSNumber numberWithInt:1];
                continue;
            }
        }
    }
    
    NSMutableDictionary* numbersByProb = [[NSMutableDictionary alloc] initWithDictionary:@{}];
    for (NSString* key in [numbersByQty allKeys]) {
        int keyAsInt = [key intValue];
        if(keyAsInt) {
            double value = (double)[[numbersByQty objectForKey:key] doubleValue]/total;
            if(!numbersByProb[[[NSNumber numberWithDouble:value] stringValue]]){
                numbersByProb[[[NSNumber numberWithDouble:value] stringValue]] = [[NSMutableArray alloc] initWithArray:@[]];
            }
            NSMutableArray* numbers = numbersByProb[[[NSNumber numberWithDouble:value] stringValue]];
            [numbers addObject:[NSNumber numberWithInt:keyAsInt]];
            [numbersByProb setObject: numbers forKey: [[NSNumber numberWithDouble:value] stringValue]];
        }
    }
    results = numbersByProb;
    keys = [ [numbersByProb allKeys] sortedArrayUsingComparator:^NSComparisonResult(id key1, id key2) {
        return [key2 compare:key1];
    }];
}

-(NSString *)getBallFromContest:(int)contest forIndex:(int)index {
    NSString* key = keys[contest];
    NSString* ball = [NSString stringWithFormat:@"%@", results[key][index]];
    return ball ? ball : @"";
}

-(nonnull NSString *)getSubtitleFromContest:(int)contest {
    return @"";
}

-(nonnull NSString *)getTitleForContest:(int)contest {
    double prob = (double)[[keys objectAtIndex: contest] doubleValue];
    NSString* value = [[NSNumber numberWithDouble: prob * 100] stringValue];
    return [NSString stringWithFormat:@"Probabilidade de: %@%%", value];
}

- (int)numberOfItemsFor:(int)index {
    NSArray* items = results[keys[index]];
    return items ? (int)[items count] : 0;
}


@end
