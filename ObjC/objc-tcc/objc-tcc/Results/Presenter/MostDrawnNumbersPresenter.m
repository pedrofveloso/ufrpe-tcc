//
//  MostDrawnNumbersPresenter.m
//  objc-tcc
//
//  Created by resource on 20/06/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

#import "MostDrawnNumbersPresenter.h"


@implementation MostDrawnNumbersPresenter {
    NSArray* results;
    Repository* repository;
}

- (instancetype)initWith:(Repository *)repo {
    if (self) {
        repository = repo;
    }
    
    return self;
}

@synthesize loteryColor;
- (UIColor*)loteryColor {
    return [Theme colorFromLotery: repository.currentLotery];
}

@synthesize numberOfSections;
- (int)numberOfSections {
    return 1;
}

- (nonnull NSString *)getBallFromContest:(int)contest forIndex:(int)index {
    return results[index];
}

- (nonnull NSString *)getSubtitleFromContest:(int)contest {
    return @"";
}

- (nonnull NSString *)getTitleForContest:(int)contest {
    return @"Números mais sorteados:";
}

- (int)numberOfItemsFor:(int)index {
    return [Theme numberOfBalls: repository.currentLotery];
}

- (void)viewDidLoad {
    results = [self mountResultsArray];
}

- (NSArray*)mountResultsArray {
    NSMutableDictionary* numbersByQty = [[NSMutableDictionary alloc] initWithDictionary:@{}];
    for (NSDictionary* dict in repository.results) {
        for (int index = 1; index <= [Theme numberOfBalls:repository.currentLotery]; index++) {
            NSString* dictKey = [NSString stringWithFormat:@"bola %d", index];
            NSString* numberAsString = [NSString stringWithFormat:@"%@", dict[dictKey]];
            if(numbersByQty[numberAsString]) {
                int key = (int)[[numbersByQty objectForKey:numberAsString] integerValue] + 1;
                numbersByQty[numberAsString] = [NSNumber numberWithLong: key];
            } else {
                numbersByQty[numberAsString] = [NSNumber numberWithInt:1];
                continue;
            }
        }
    }

    NSArray* resultArray = [numbersByQty keysSortedByValueUsingComparator:^NSComparisonResult(id dict1, id dict2) {
        return [dict2 compare:dict1];
    }];
    
    return resultArray;
}

@end
