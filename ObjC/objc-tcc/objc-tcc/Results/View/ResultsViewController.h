//
//  ResultsViewController.h
//  objc-tcc
//
//  Created by resource on 19/06/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "../Presenter/ResultPresenterDelegate.h"

NS_ASSUME_NONNULL_BEGIN

@interface ResultsViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
@property NSObject<ResultPresenterDelegate>* presenter;
@end

NS_ASSUME_NONNULL_END
