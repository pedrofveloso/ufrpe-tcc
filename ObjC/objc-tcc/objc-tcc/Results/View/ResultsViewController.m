//
//  ResultsViewController.m
//  objc-tcc
//
//  Created by resource on 19/06/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

#import "ResultsViewController.h"
#import "BallCollectionViewCell.h"
#import "../../CollectionComponents/HeaderCollectionReusableView.h"

@interface ResultsViewController ()
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation ResultsViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    return [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [_presenter viewDidLoad];
    [self setupComponents];
}

- (void) setupComponents {
    _collectionView.dataSource = self;
    _collectionView.delegate = self;
    self.navigationItem.title = @"Resultados";
    [_collectionView registerNib:[UINib nibWithNibName:@"BallCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"BallCollectionViewCell"];
    [_collectionView registerNib:[UINib nibWithNibName:@"HeaderCollectionReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"resultView"];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    int count = [_presenter numberOfItemsFor:(int) section];
    return count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    BallCollectionViewCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BallCollectionViewCell" forIndexPath:indexPath];
    NSString* ballNumber = [_presenter getBallFromContest:(int)indexPath.section forIndex:(int)indexPath.row];
    [cell setupComponentWithNumber: ballNumber andColor: _presenter.loteryColor];
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    HeaderCollectionReusableView* header = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"resultView" forIndexPath:indexPath];
    NSString* title = [_presenter getTitleForContest: (int)indexPath.section];
    NSString* subtitle = [_presenter getSubtitleFromContest: (int)indexPath.section];
    [header setupWithTitle: title andSubtitle: subtitle];
    return header;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return [_presenter numberOfSections];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    return CGSizeMake(collectionView.frame.size.width, 40.0);
}

@end
