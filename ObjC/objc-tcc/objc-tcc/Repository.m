//
//  Repository.m
//  objc-tcc
//
//  Created by resource on 19/06/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

#import "Repository.h"

@implementation Repository

- (instancetype)initWithLotery: (enum Lotery) lotery
{
    self = [super init];
    if (self) {
        _currentLotery = lotery;
        NSString* resource = [NSString stringWithFormat:@"resultados-%@", [Theme stringFromLotery:lotery]];
        NSString* path = [NSBundle.mainBundle pathForResource:resource ofType:@"json"];
        if (path == nil) {
            _results = @[];
            return self;
        }
        NSData* data = [[NSData alloc] initWithContentsOfFile: path];
        NSJSONSerialization* json = [NSJSONSerialization JSONObjectWithData: data options: NSJSONReadingMutableLeaves error:nil];
        if (json) {
            _results = (NSArray*)json;
        } else {
            [NSException raise:@"InvalidJsonFile" format:@""];
        }
    }
    return self;
}

@end

